# FROM debian:10-slim
# FROM registry.s.rosatom.education/sirius/docker/debian:10-slim
# FROM registry.s.rosatom.education/sirius/docker/ubuntu:22.04
# FROM registry.s.rosatom.education/sirius/docker/ubuntu:20.04  #если без указания версии, то по умолчанию возьмет самую свежую версию
FROM registry.s.rosatom.education/sirius/docker/python:3.11-alpine


ARG USER=app
ARG ID=2000

RUN addgroup -g ${ID} ${USER} && \
    adduser -D -H -G ${USER} -u ${ID} -h /nonexistent -s /sbin/nologin ${USER}

# RUN apt-get update && \
#     apt-get install -y --no-install-recommends \
#     python3 python3-pip python3-setuptools && \
#     rm -rf /var/cache/apt/archives/*

ADD ./app/requirements.txt /requirements.txt
RUN pip3 install -r /requirements.txt

COPY ./app /app/

EXPOSE 80

WORKDIR /app

USER ${USER}

CMD [ "flask", "run", "--host=0.0.0.0", "--port=80" ]

